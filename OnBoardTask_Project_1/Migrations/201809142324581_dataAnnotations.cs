namespace OnBoardTask_Project_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dataAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "CustomerName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Customers", "CustomerAddress", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Products", "ProductName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Stores", "StoreName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Stores", "StoreAddress", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.ProductSolds", "ProductId");
            CreateIndex("dbo.ProductSolds", "CustomerId");
            CreateIndex("dbo.ProductSolds", "StoreId");
            AddForeignKey("dbo.ProductSolds", "CustomerId", "dbo.Customers", "CustomerId", cascadeDelete: true);
            AddForeignKey("dbo.ProductSolds", "ProductId", "dbo.Products", "ProductId", cascadeDelete: true);
            AddForeignKey("dbo.ProductSolds", "StoreId", "dbo.Stores", "StoreId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductSolds", "StoreId", "dbo.Stores");
            DropForeignKey("dbo.ProductSolds", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductSolds", "CustomerId", "dbo.Customers");
            DropIndex("dbo.ProductSolds", new[] { "StoreId" });
            DropIndex("dbo.ProductSolds", new[] { "CustomerId" });
            DropIndex("dbo.ProductSolds", new[] { "ProductId" });
            AlterColumn("dbo.Stores", "StoreAddress", c => c.String());
            AlterColumn("dbo.Stores", "StoreName", c => c.String());
            AlterColumn("dbo.Products", "ProductName", c => c.String());
            AlterColumn("dbo.Customers", "CustomerAddress", c => c.String());
            AlterColumn("dbo.Customers", "CustomerName", c => c.String());
        }
    }
}
