namespace OnBoardTask_Project_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedDataType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductSolds", "DateSold", c => c.DateTime(nullable: false));
            DropColumn("dbo.ProductSolds", "DataSold");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductSolds", "DataSold", c => c.String());
            DropColumn("dbo.ProductSolds", "DateSold");
        }
    }
}
