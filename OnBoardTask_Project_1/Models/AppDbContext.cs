﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace OnBoardTask_Project_1.Models
{
	public class AppDbContext:DbContext
	{
		public AppDbContext() : base("name=Project_1_Db") { }

		public DbSet<Customer> Customers { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Store> Stores { get; set; }
		public DbSet<ProductSold> ProductsSold { get; set; }

		// The modelBuilder.Conventions.Remove statement in the OnModelCreating method prevents
		// table names from being pluralized. If you didn't do this, the generated tables in the 
		// database would be named Customers, Products, Stores and ProductSolds. Instead, the table names 
		// will be Customer, Product, Store and ProductSold.

		/*protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}*/
	}
}