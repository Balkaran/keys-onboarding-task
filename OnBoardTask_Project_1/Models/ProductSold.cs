﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OnBoardTask_Project_1.Models
{
	public class ProductSold
	{
		[Key]
		public int ProductSoldId { get; set; }
		public int ProductId { get; set; }
		public int CustomerId { get; set; }
		public int StoreId { get; set; }

		[DataType(DataType.Date)]
		public DateTime DateSold { get; set; }

		public virtual Customer Customer { get; set; }
		public virtual Product Product { get; set; }
		public virtual Store Store { get; set; }
	}
}