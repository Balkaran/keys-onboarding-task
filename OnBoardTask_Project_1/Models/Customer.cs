﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OnBoardTask_Project_1.Models
{
	public class Customer
	{
		[Key]
		public int CustomerId { get; set; }

		[Required(ErrorMessage = "Customer Name is required")]
		[StringLength(100, MinimumLength = 3)]
		public string CustomerName { get; set; }

		[Required(ErrorMessage = "Customer Address is required")]
		[StringLength(200, MinimumLength = 5)]
		public string CustomerAddress { get; set; }

		public virtual ICollection<ProductSold> ProductsSold { get; set; }
	}
}